import { 
	postDataLogin, 
	postDataDaftar,
	postCekAkun,
	// postKelompokUji
} from '@/config/auth'
import store from './index'
import axios from 'axios'

var dataAuth = {
	namespaced: true,
	state: {
		statusCekNomor: null,
		modalKelompokUji: false,
		dataUser: ''
	},
	mutations: {
		updateStatusCekNomor (state, payload) {
			state.statusCekNomor = payload
		},
		updateModalKelompokUji (state, payload) {
			state.modalKelompokUji = payload
		},
		updateDataUser (state, payload) {
			state.dataUser = payload
		}
	},
	getters: {
		getDataUser: state => state.dataUser
	},
	actions: {
		updateModalKelompokUji (context, payload) {
			context.commit('updateModalKelompokUji', payload)
		},
		postDataLogin (context, payload) {
      store.dispatch('updateLoading', true)
			postDataLogin (payload)
				.then(res => {
					if (res.data.status === 200) {
						store.dispatch('updateNotif', {
              visible: true,
              status: true,
              msg: 'Login berhasil'
            })
            store.dispatch('updateLoading', false)
						
						const kelompokUji = res.data.data.kelompokUji
						if (kelompokUji !== null) {
							window.localStorage.setItem('dataPeserta', JSON.stringify(res.data.data))
							window.localStorage.setItem('token', res.data.data.token)
							window.location.reload()
						} else {
							context.commit('updateDataUser', res.data.data)
							context.dispatch('updateModalKelompokUji', true)
						}
					} else {
						store.dispatch('updateNotif', {
              visible: true,
              status: false,
              msg: 'Login gagal'
            })
            store.dispatch('updateLoading', false)
					}
				})
				.catch(() => {
					store.dispatch('updateNotif', {
            visible: true,
            status: false,
            msg: 'Login gagal'
          })
          store.dispatch('updateLoading', false)
				})
		},
		postKelompokUji (context, payload) {
			const http = axios.create({
				baseURL: 'https://app.suksesutbk.com/api/v2/',
				headers: {
					'Authorization': 'Bearer '+ context.getters.getDataUser.token,
				}
			})
			http.post('kelompokUji', payload)
				.then(res => {
					if (res.status === 200) {
						window.localStorage.setItem('dataPeserta', JSON.stringify(context.getters.getDataUser))
						window.localStorage.setItem('token', context.getters.getDataUser.token)
						window.location.reload()
					} else {
						store.dispatch('updateNotif', {
              visible: true,
              status: false,
              msg: 'Proses gagal'
            })
					}
				})
				.catch(() => {
					store.dispatch('updateNotif', {
            visible: true,
            status: false,
            msg: 'Proses gagal'
          })
				})
		},
		postDataDaftar (context, payload) {
      store.dispatch('updateLoading', true)
			var nohp = {
				nohp: payload.nohp
			}
			postCekAkun(nohp)
				.then(res => {
					if (res.status === 200) {
						if (res.data.tersedia) {
							postDataDaftar (payload)
								.then(res => {
									if (res.status === 201) {
										window.localStorage.setItem('dataPeserta', JSON.stringify(res.data.data))
                    window.localStorage.setItem('token', res.data.data.token)
                    store.dispatch('updateNotif', {
                      visible: true,
                      status: true,
                      msg: 'Pendaftaran berhasil'
                    })
                    store.dispatch('updateLoading', false)
										window.location.reload()									
									} else {
                    store.dispatch('updateNotif', {
                      visible: true,
                      status: false,
                      msg: 'Pendaftaran gagal'
                    })
                    store.dispatch('updateLoading', false)
									}
								})
								.catch(() => {
                  store.dispatch('updateNotif', {
                    visible: true,
                    status: false,
                    msg: 'Pendaftaran gagal'
                  })
                  store.dispatch('updateLoading', false)
								})
						} else {
							store.dispatch('updateNotif', {
                visible: true,
                status: false,
                msg: 'Nomor HP sudah pernah dipakai'
              })
              store.dispatch('updateLoading', false)
						}
					}
				})
				.catch(() => {
          store.dispatch('updateNotif', {
            visible: true,
            status: false,
            msg: 'Nomor HP sudah pernah dipakai'
          })
          store.dispatch('updateLoading', false)
				})
		}
	}
}

export default dataAuth